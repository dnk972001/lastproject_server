﻿using DummyClient;
using ServerCore;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;


public class NetworkManager : MonoBehaviour
{
	ServerSession _session = new ServerSession();



    void Start()
    {
		// DNS (Domain Name System)
		string host = Dns.GetHostName();
		IPHostEntry ipHost = Dns.GetHostEntry(host);
		IPAddress ipAddr = IPAddress.Parse("127.0.0.1");
		IPEndPoint endPoint = new IPEndPoint(ipAddr, 9090);

		Connector connector = new Connector();

		Debug.Log($"클라이언트 시작");

		connector.Connect(endPoint,
			() => { return _session; },
			1);


	}

    void Update()
    {
    }
}
