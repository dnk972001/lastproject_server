﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using ServerCore;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;



[Serializable]
struct PleaseSuccess
{
	public ushort size;
	public ushort type;
	public int inta;
	public float floatx;
	public float floaty;
	public string ID;

	public static PleaseSuccess FromArray(byte[] bytes)
	{
		var reader = new BinaryReader(new MemoryStream(bytes));

		var s = default(PleaseSuccess);

		s.size = reader.ReadUInt16();
		s.type = reader.ReadUInt16();
		s.inta = (int)reader.ReadUInt32();
		s.floatx = reader.ReadSingle();
		s.floaty = reader.ReadSingle();
		s.ID = reader.ReadString();
		return s;
	}
};

namespace DummyClient
{
	class ServerSession : PacketSession
	{

		public override void OnConnected(EndPoint endPoint)
		{
			Console.WriteLine($"OnConnected : {endPoint}");			
		}

		public override void OnDisconnected(EndPoint endPoint)
		{
			Console.WriteLine($"OnDisconnected : {endPoint}");
		}

		public override void OnRecvPacket(ArraySegment<byte> buffer)
		{
			//PacketManager.Instance.OnRecvPacket(this, buffer);

			//string recvData = Encoding.UTF8.GetString(buffer.Array, buffer.Offset, buffer.Count);
			//Console.WriteLine($"[From Server] {recvData}");
			ushort count = 0;
			ushort size = BitConverter.ToUInt16(buffer.Array, buffer.Offset);
			count += 2;
			ushort id = BitConverter.ToUInt16(buffer.Array, buffer.Offset + count);
			count += 2;
			int inta = BitConverter.ToUInt16(buffer.Array, buffer.Offset + count);
			count += 4;
			float floatx = BitConverter.ToSingle(buffer.Array, buffer.Offset + count);
			count += 4;
			float floaty = BitConverter.ToSingle(buffer.Array, buffer.Offset + count);
			count += 4;
			string IDs = BitConverter.ToString(buffer.Array, buffer.Offset + count, 20);

			string utfString = Encoding.UTF8.GetString(buffer.Array, buffer.Offset + count, 20);


			// ASCII conversion - string from bytes  
			string asciiString = Encoding.ASCII.GetString(buffer.Array, buffer.Offset + count, 20);


			//IDs = Encoding.Default.GetString(buffer.Array).TrimEnd('\0');

			Debug.Log($"[From Server] size: {size} type : {id} inta : {inta}  floatx: {floatx} floaty: {floaty} ID :{utfString}");
			Debug.Log(utfString);
			Debug.Log(asciiString);
			byte[] ret = new byte[size];

			Array.Copy(buffer.Array, ret, size);


			Debug.Log(ret.Length);


			ArraySegment<byte> sem =  new ArraySegment<byte>(ret);


			Send(sem);


			//PleaseSuccess sP = new PleaseSuccess();



			//Debug.Log(message: $"[From Server] size: {sP.size} type : {sP.type} int : {sP.inta} floatx: {sP.floatx} float.y : {sP.floaty} id: {sP.ID}");


		}

		public override void OnSend(int numOfBytes)
		{
			//Console.WriteLine($"Transferred bytes: {numOfBytes}");
		}

		public static T ByteToStruct<T>(byte[] buffer) where T : struct
		{
			int size = Marshal.SizeOf(typeof(T));
			if (size > buffer.Length)
			{
				throw new Exception();
			}

			IntPtr ptr = Marshal.AllocHGlobal(size);
			Marshal.Copy(buffer, 0, ptr, size);
			T obj = (T)Marshal.PtrToStructure(ptr, typeof(T));
			Marshal.FreeHGlobal(ptr);
			return obj;
		}


	}
}
