using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerIngamePlayerManager : ServerIngameManager
{
    public static ServerIngamePlayerManager instance;

    [SerializeField] GameObject KarmenObj;
    [SerializeField] GameObject JadeObj;
    [SerializeField] GameObject LeinaObj;
    [SerializeField] GameObject EvaObj;

    public string ID;
    public int list;

    private bool isTag;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        //if (list == 0)
        //{
        //    if (ServerLoginManager.playerList[0].selectMainCharacter == 1)
        //    {
        //        character1[0] = KarmenObj;
        //        KarmenObj.tag = "MainCharacter";
        //        KarmenObj.SetActive(true);
        //    }
        //    else if (ServerLoginManager.playerList[0].selectMainCharacter == 2)
        //    {
        //        character1[0] = JadeObj;
        //        JadeObj.tag = "MainCharacter";
        //        JadeObj.SetActive(true);
        //    }
        //    else if (ServerLoginManager.playerList[0].selectMainCharacter == 3)
        //    {
        //        character1[0] = LeinaObj;
        //        LeinaObj.tag = "MainCharacter";
        //        LeinaObj.SetActive(true);
        //    }
        //    else if (ServerLoginManager.playerList[0].selectMainCharacter == 4)
        //    {
        //        character1[0] = EvaObj;
        //        EvaObj.tag = "MainCharacter";
        //        EvaObj.SetActive(true);
        //    }

        //    if (ServerLoginManager.playerList[0].selectSubCharacter == 1)
        //    {
        //        character2[0] = KarmenObj;
        //        KarmenObj.tag = "SubCharacter";
        //        KarmenObj.SetActive(true);
        //    }
        //    else if (ServerLoginManager.playerList[0].selectSubCharacter == 2)
        //    {
        //        character2[0] = JadeObj;
        //        JadeObj.tag = "SubCharacter";
        //        JadeObj.SetActive(true);
        //    }
        //    else if (ServerLoginManager.playerList[0].selectSubCharacter == 3)
        //    {
        //        character2[0] = LeinaObj;
        //        LeinaObj.tag = "SubCharacter";
        //        LeinaObj.SetActive(true);
        //    }
        //    else if (ServerLoginManager.playerList[0].selectSubCharacter == 4)
        //    {
        //        character2[0] = EvaObj;
        //        EvaObj.tag = "SubCharacter";
        //        EvaObj.SetActive(true);
        //    }
        //    ID = ServerLoginManager.playerList[0].playerID;
        //    StartCoroutine("CoSendPacket");

        //    Debug.Log(ID);
        //}
        //else
        //{
        //    if (ServerLoginManager.playerList[list].playerID != null)
        //    {
        //        ID = ServerLoginManager.playerList[list].playerID;
        //        if (ServerLoginManager.playerList[list].selectMainCharacter == 1)
        //        {
        //            character1[list] = KarmenObj;
        //            KarmenObj.tag = "MainCharacter";
        //            KarmenObj.SetActive(true);
        //        }
        //        else if (ServerLoginManager.playerList[list].selectMainCharacter == 2)
        //        {
        //            character1[list] = JadeObj;
        //            JadeObj.tag = "MainCharacter";
        //            JadeObj.SetActive(true);
        //        }
        //        else if (ServerLoginManager.playerList[list].selectMainCharacter == 3)
        //        {
        //            character1[list] = LeinaObj;
        //            LeinaObj.tag = "MainCharacter";
        //            LeinaObj.SetActive(true);
        //        }
        //        else if (ServerLoginManager.playerList[list].selectMainCharacter == 4)
        //        {
        //            character1[list] = EvaObj;
        //            EvaObj.tag = "MainCharacter";
        //            EvaObj.SetActive(true);
        //        }

        //        if (ServerLoginManager.playerList[list].selectSubCharacter == 1)
        //        {
        //            character2[list] = KarmenObj;
        //            KarmenObj.tag = "SubCharacter";
        //            KarmenObj.SetActive(true);
        //        }
        //        else if (ServerLoginManager.playerList[list].selectSubCharacter == 2)
        //        {
        //            character2[list] = JadeObj;
        //            JadeObj.tag = "SubCharacter";
        //            JadeObj.SetActive(true);
        //        }
        //        else if (ServerLoginManager.playerList[list].selectSubCharacter == 3)
        //        {
        //            character2[list] = LeinaObj;
        //            LeinaObj.tag = "SubCharacter";
        //            LeinaObj.SetActive(true);
        //        }
        //        else if (ServerLoginManager.playerList[list].selectSubCharacter == 4)
        //        {
        //            character2[list] = EvaObj;
        //            EvaObj.tag = "SubCharacter";
        //            EvaObj.SetActive(true);
        //        }

        //        Debug.Log(ID);
        //        Debug.Log(ServerLoginManager.playerList[list].selectMainCharacter);
        //        Debug.Log(ServerLoginManager.playerList[list].selectSubCharacter);
        //    }
        //}
        
        isTag = true;
    }

    void Update()
    {
        if (list == 0)
        {
            if (Input.GetKey(KeyCode.F))
            {
                ServerMainSubTag();
            }
        }
        //else
        //{
        //    for (int i = 1; i < 4; ++i)
        //    {
        //        if (character1.gameObject.tag == "MainCharacter")
        //        {
        //            character1.gameObject.transform.position = ServerLoginManager.playerList[i].mainCharacterPos;
        //            character1.gameObject.transform.rotation = ServerLoginManager.playerList[i].mainCharacterRot;
        //        }
        //        else if (character2.gameObject.tag == "MainCharacter")
        //        {
        //            character2.gameObject.transform.position = ServerLoginManager.playerList[i].mainCharacterPos;
        //            character2.gameObject.transform.rotation = ServerLoginManager.playerList[i].mainCharacterRot;
        //        }
        //    }
        //}
    }

    public void ServerMainSubTag()
    {
        // main->sub
        //if (isTag)
        //{
        //    character1.gameObject.tag = "SubCharacter";
        //    character2.gameObject.tag = "MainCharacter";
        //    isTag = false;
        //}
        //else
        //{
        //    character1.gameObject.tag = "MainCharacter";
        //    character2.gameObject.tag = "SubCharacter";
        //    isTag = true;
        //}
    }

    //IEnumerator CoSendPacket()
    //{
    //    while (true)
    //    {
    //        yield return new WaitForSeconds(0.1f);
    //        cs_PlayerData movePacket = new cs_PlayerData();

    //        movePacket.ID = ServerLoginManager.playerList[0].playerID;

    //        if (character1[0].gameObject.tag == "MainCharacter")
    //        {
    //            //movePacket.mainPlayer_Behavior = ServerLoginManager.playerList[0].mainCharacterBehavior;
    //            movePacket.mainPlayer_Pos_X = character1[0].gameObject.transform.position.x;
    //            movePacket.mainPlayer_Pos_Z = character1[0].gameObject.transform.position.z;
    //            movePacket.mainPlayer_Rot_Y = character1[0].gameObject.transform.rotation.y;

    //            //Debug.Log(character1.gameObject.transform.position);

    //            //movePacket.subPlayer_Pos_X = character1.gameObject.transform.position.x;
    //            //movePacket.subPlayer_Pos_Z = character1.gameObject.transform.position.z;
    //            //movePacket.subPlayer_Rot_Y = character1.gameObject.transform.rotation.y;
    //        }
    //        else if (character2[0].gameObject.tag == "MainCharacter")
    //        {
    //            //movePacket.mainPlayer_Behavior = ServerLoginManager.playerList[0].mainCharacterBehavior;
    //            movePacket.mainPlayer_Pos_X = character2[0].gameObject.transform.position.x;
    //            movePacket.mainPlayer_Pos_Z = character2[0].gameObject.transform.position.z;
    //            movePacket.mainPlayer_Rot_Y = character2[0].gameObject.transform.rotation.y;

    //            //movePacket.subPlayer_Pos_X = character2.gameObject.transform.position.x;
    //            //movePacket.subPlayer_Pos_Z = character2.gameObject.transform.position.z;
    //            //movePacket.subPlayer_Rot_Y = character2.gameObject.transform.rotation.y;
    //        }

    //        //Debug.Log("Send Packet"+ movePacket.ID+ " " + movePacket.mainPlayer_Pos_X + " " + movePacket.mainPlayer_Pos_Z);

    //        NetworkManager.instance.Send(movePacket.Write());
    //        //Debug.Log(pos.z);
    //    }
    //}
}