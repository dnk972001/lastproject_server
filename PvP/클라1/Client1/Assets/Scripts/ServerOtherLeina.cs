using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerOtherLeina : MonoBehaviour
{
    [SerializeField] GameObject arrow;
    [SerializeField] Transform arrowPos;

    private Animator otherAnimator;
    private int preBehavior;

    void Start()
    {
        otherAnimator = GetComponent<Animator>();
        preBehavior = 0;
    }

    void Update()
    {
        if (ServerOtherPlayerManager.instance.is_MainCharacter == 1)
            AnimationControl();
    }

    public void AnimationControl()
    {
        if (ServerLoginManager.playerList[ServerOtherPlayerManager.instance.index].mainCharacterBehavior == 0)
        {
            otherAnimator.SetBool("Run", false);
        }
        else if (ServerLoginManager.playerList[ServerOtherPlayerManager.instance.index].mainCharacterBehavior == 1)
        {
            otherAnimator.SetBool("Run", true);
            preBehavior = 1;
        }
        else if (ServerLoginManager.playerList[ServerOtherPlayerManager.instance.index].mainCharacterBehavior == 2)
        {
            if (preBehavior != 2)
                StartCoroutine(DodgeDelay());
        }
        else if (ServerLoginManager.playerList[ServerOtherPlayerManager.instance.index].mainCharacterBehavior == 3)
        {
            if (preBehavior != 3)
                StartCoroutine(ShootArrow());
        }
    }

    IEnumerator ShootArrow()
    {
        preBehavior = 3;
        otherAnimator.SetTrigger("Attack");
        GameObject instantArrow = Instantiate(arrow, arrowPos.position, arrowPos.rotation);
        Rigidbody arrowRigid = instantArrow.GetComponent<Rigidbody>();
        arrowRigid.velocity = arrowPos.forward;
        yield return new WaitForSeconds(0.5f);
        preBehavior = 0;
    }

    IEnumerator DodgeDelay()
    {
        preBehavior = 2;
        otherAnimator.SetTrigger("Dodge");
        yield return new WaitForSeconds(1.0f);
        preBehavior = 0;
    }
}
