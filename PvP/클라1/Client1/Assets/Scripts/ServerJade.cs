using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ServerJade : SubAI
{
    [SerializeField] GameObject useAssaultRifle;
    [SerializeField] GameObject backAssaultRifle;

    [SerializeField] Transform assaultRifleBulletPos;
    [SerializeField] GameObject assaultRifleBullet;

    public float moveSpeed = 5.0f;
    public float dodgeCoolTime = 7.0f;
    public float fireDelay = 0.5f;
    public float subFireDelay = 1.5f;
    public float followDistance = 5.0f;

    float curDodgeCoolTime;
    float curFireDelay;

    bool canMove;
    bool canDodge;
    bool canAttack;

    bool onDodge;

    float distanceWithPlayer;

    Vector3 vecTarget;

    Animator anim;
   
    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        vecTarget = transform.position;

        curDodgeCoolTime = 0;

        canMove = true;
        canDodge = false;
        canAttack = false;

        onDodge = true;

        curFireDelay = fireDelay;
    }

    void Update()
    {
        curFireDelay += Time.deltaTime;
        if (gameObject.transform.tag == "MainCharacter")
        {
            if (canMove)
                Move();
            if (canAttack)
                Attack();
            if (canDodge)
                Dodge();
            Stop();
            CoolTime();

            //for (int i = 0; i < 4; ++i)
            //{
            //    if (i == 0)
            //    {
            //        ServerLoginManager.playerList[i].mainCharacterBehavior = 1;
            //        ServerLoginManager.playerList[i].mainCharacterPos = transform.position;
            //        ServerLoginManager.playerList[i].mainCharacterRot = transform.rotation;

            //    }
            //    else
            //    {
            //        transform.position = ServerLoginManager.playerList[i].mainCharacterPos;
            //        transform.rotation = ServerLoginManager.playerList[i].mainCharacterRot;
            //    }
            //}
        }
        else if (gameObject.transform.tag == "SubCharacter")
        {
            //distance = Vector3.Distance(tagCharacter.transform.position, transform.position);

            //if (currentState == characterState.trace)
            //{
            //    MainCharacterTrace();
            //    anim.SetBool("Run", true);
            //    curFireDelay = 1f;
            //}
            //else if (currentState == characterState.attack)
            //{
            //    //SubAttack();

            //    //if (target)
            //    //{
            //    //    Quaternion lookRotation = Quaternion.LookRotation(target.transform.position - transform.position);
            //    //    Vector3 euler = Quaternion.RotateTowards(transform.rotation, lookRotation, spinSpeed * Time.deltaTime).eulerAngles;
            //    //    transform.rotation = Quaternion.Euler(0, euler.y, 0);

            //    //}
            //    //if (curFireDelay > subFireDelay && target != null)
            //    //{
            //    //    GameObject instantBullet = Instantiate(assaultRifleBullet, assaultRifleBulletPos.position, assaultRifleBulletPos.rotation);
            //    //    Rigidbody bulletRigid = instantBullet.GetComponent<Rigidbody>();
            //    //    bulletRigid.velocity = assaultRifleBulletPos.forward;

            //    //    moveSpeed = 0f;
            //    //    anim.SetBool("Run", false);
            //    //    vecTarget = transform.position;

            //    //    anim.SetTrigger("shootAssaultRifle");
            //    //    curFireDelay = 0;

            //    //    StartCoroutine(AttackDelay());
            //    //}
            //}
            //else if (currentState == characterState.idle)
            //{
            //    Idle();
            //    anim.SetBool("Run", false);
            //    curFireDelay = 1f;
            //}
        }
        Tag();
    }
    void Move()
    {
       if (Input.GetMouseButton(1))
        {
            moveSpeed = 5.0f;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                vecTarget = hit.point;
                vecTarget.y = transform.position.y;

                Vector3 nextVec = hit.point - transform.position;
                nextVec.y = 0;
                transform.LookAt(transform.position + nextVec);
            }
        }
        transform.position = Vector3.MoveTowards(transform.position, vecTarget, moveSpeed * Time.deltaTime);
        anim.SetBool("Run", vecTarget != transform.position);

        ServerLoginManager.playerList[0].mainCharacterBehavior = 1;

        if(vecTarget == transform.position)
            ServerLoginManager.playerList[0].mainCharacterBehavior = 0;
    }
    void Stop()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            moveSpeed = 0f;
            anim.SetBool("Run", false);
            vecTarget = transform.position;
            ServerLoginManager.playerList[0].mainCharacterBehavior = 0;
        }
    }
    void Dodge()
    {
        if (Input.GetKeyDown(KeyCode.Space) && onDodge)
        {
            onDodge = false;

            canAttack = false;
            canMove = false;

            curDodgeCoolTime = 0.0f;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                Vector3 nextVec = hit.point - transform.position;
                nextVec.y = 0;
                transform.LookAt(transform.position + nextVec);
            }

            moveSpeed *= 2;
            anim.SetTrigger("Dodge");

            StartCoroutine(DodgeDelay());
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("DodgeForward"))
        {
            transform.Translate(Vector3.forward * 5 * Time.deltaTime);
            vecTarget = transform.position;
            anim.SetBool("Run", false);
        }
    }
    void Attack()
    {
        if (Input.GetMouseButtonDown(0))
        {
            canMove = false;
            canDodge = false;

            if (curFireDelay > fireDelay)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    Vector3 nextVec = hit.point - transform.position;
                    nextVec.y = 0;
                    transform.LookAt(transform.position + nextVec);
                }

                //SoundManager.instance.SFXPlay("Attack", attackClip);

                GameObject instantBullet = Instantiate(assaultRifleBullet, assaultRifleBulletPos.position, assaultRifleBulletPos.rotation);
                Rigidbody bulletRigid = instantBullet.GetComponent<Rigidbody>();
                bulletRigid.velocity = assaultRifleBulletPos.forward;
                //var bullet = ObjectPooling.GetObject();
                //bullet.transform.position = assaultRifleBulletPos.position;
                //bullet.transform.rotation = assaultRifleBulletPos.rotation;

                moveSpeed = 0f;
                anim.SetBool("Run", false);
                vecTarget = transform.position;

                anim.SetTrigger("shootAssaultRifle");
                curFireDelay = 0;

                StartCoroutine(AttackDelay());

                ServerLoginManager.playerList[0].mainCharacterBehavior = 2;
            }
        }
    }
    void CoolTime()
    {
        if (curDodgeCoolTime < dodgeCoolTime)
        {
            curDodgeCoolTime += Time.deltaTime;
        }
        else
        {
            onDodge = true;
        }
    }

    void Tag()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            vecTarget = transform.position;
        }
    }
    IEnumerator AttackDelay()
    {
        yield return new WaitForSeconds(0.2f);
        canMove = true;
        canDodge = true;
    }

    IEnumerator DodgeDelay()
    {
        yield return new WaitForSeconds(1.0f);
        canAttack = true;
        canMove = true;
    }
}