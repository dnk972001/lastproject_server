//TCP서버
// 소켓을 사용하기 위해서 라이브러리 참조해야 한다.
#pragma comment(lib, "ws2_32")
#pragma warning(disable:4996)
#include <stdio.h>
#include <iostream>
#include <vector>
#include <thread>
#include <string.h>
// 소켓을 사용하기 위한 라이브러리
#include <WinSock2.h>
#include <sqlext.h>
#include <Windows.h>

// 수신 버퍼 사이즈
#define BUFFERSIZE 1024
#define PORT 9090
using namespace std;

struct TypeCheck {
    int type;
};

struct pleaseSuccess {
    USHORT size;
    USHORT type;
    int inta;
    float floatx;
    float floaty;
    char ID[20];
};

void client(SOCKET clientSock, SOCKADDR_IN clientAddr, vector<thread*>* clientlist)
{
    // 접속 정보를 콘솔에 출력한다.
    cout << "Client connected IP address = " << inet_ntoa(clientAddr.sin_addr) << ":" << ntohs(clientAddr.sin_port) << endl;


    pleaseSuccess sP;
    sP.size = sizeof(sP);
    sP.type = 7;
    sP.inta = 1234;
    sP.floatx = 34.234f;
    sP.floaty = 324.523f;
    char idtemp[20] = "dnk97";
    strncpy(sP.ID, idtemp, 20);

    cout << "Size: " << sP.size << "Type: " << sP.type << "inta: " << sP.inta << "floatx: " << sP.floatx << "floaty: " << sP.floaty << "id: " << sP.ID  << endl;
    const wchar_t* message;
    message = (const wchar_t*)&sP;
    int sign;
    //while (true) {
        //sign = sign = send(clientSock, (char*)message, sizeof(sP), 0);
        //Sleep(1000);
    //}
    cout << "데이터 송신:" << sign << endl;

    char msg[BUFFERSIZE];
    sign = recv(clientSock, (char*)&msg, BUFFERSIZE, 0);

    pleaseSuccess* typecheck = reinterpret_cast<pleaseSuccess*>(msg);

    cout << "데이터 수신: " << sign << endl;

    cout << typecheck->size << endl;
    cout << typecheck->type << endl;
    cout << typecheck->inta << endl;
    cout << typecheck->floatx << endl;
    cout << typecheck->floaty << endl;
    cout << typecheck->ID << endl;


    closesocket(clientSock);
    // 접속 정보를 콘솔에 출력한다.
    cout << "Client disconnected IP address = " << inet_ntoa(clientAddr.sin_addr) << ":" << ntohs(clientAddr.sin_port) << endl;
    // threadlist에서 현재 쓰레드를 제거한다.
    for (auto ptr = clientlist->begin(); ptr < clientlist->end(); ptr++)
    {
        // thread 아이디가 같은 것을 찾아서
        if ((*ptr)->get_id() == this_thread::get_id())
        {
            // 리스트에서 뺀다.
            clientlist->erase(ptr);
            break;
        }
    }
    // thread 메모리 해지는 thread가 종료 됨으로 자동으로 처리된다.
}

// 실행 함수
int main()
{
    // 클라이언트 접속 중인 client list
    vector<thread*> clientlist;
    // 소켓 정보 데이터 설정
    WSADATA wsaData;
    // 소켓 실행.
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        return 1;
    }
    // Internet의 Stream 방식으로 소켓 생성
    SOCKET serverSock = socket(PF_INET, SOCK_STREAM, 0);
    // 소켓 주소 설정
    SOCKADDR_IN addr;
    // 구조체 초기화
    memset(&addr, 0, sizeof(addr));
    // 소켓은 Internet 타입
    addr.sin_family = AF_INET;
    // 서버이기 때문에 local 설정한다.
    // Any인 경우는 호스트를 127.0.0.1로 잡아도 되고 localhost로 잡아도 되고 양쪽 다 허용하게 할 수 있따. 그것이 INADDR_ANY이다.
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    // 서버 포트 설정...저는 9090으로 설정함.
    addr.sin_port = htons(PORT);
    // 설정된 소켓 정보를 소켓에 바인딩한다.
    if (bind(serverSock, (SOCKADDR*)&addr, sizeof(SOCKADDR_IN)) == SOCKET_ERROR)
    {
        // 에러 콘솔 출력
        cout << "error" << endl;
        return 1;
    }
    // 소켓을 대기 상태로 기다린다.
    if (listen(serverSock, SOMAXCONN) == SOCKET_ERROR)
    {
        // 에러 콘솔 출력
        cout << "error" << endl;
        return 1;
    }
    // 서버를 시작한다.
    cout << "Server Start" << endl;
    // 다중 접속을 위해 while로 소켓을 대기한다.
    while (1)
    {
        // 접속 설정 구조체 사이즈
        int len = sizeof(SOCKADDR_IN);
        // 접속 설정 구조체
        SOCKADDR_IN clientAddr;
        // client가 접속을 하면 SOCKET을 받는다.
        SOCKET clientSock = accept(serverSock, (SOCKADDR*)&clientAddr, &len);
        // 쓰레드를 실행하고 쓰레드 리스트에 넣는다.
        clientlist.push_back(new thread(client, clientSock, clientAddr, &clientlist));
    }
    // 종료가 되면 쓰레드 리스트에 남아 있는 쓰레드를 종료할 때까지 기다린다.
    if (clientlist.size() > 0)
    {
        for (auto ptr = clientlist.begin(); ptr < clientlist.end(); ptr++)
        {
            (*ptr)->join();
        }
    }
    // 서버 소켓 종료
    closesocket(serverSock);
    // 소켓 종료
    WSACleanup();
    return 0;
}

