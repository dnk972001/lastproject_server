﻿////Client Example
////TCP
//using System;
//using System.Threading;
//using System.Linq;
//using System.Text;
//using System.Net;
//using System.Net.Sockets;
//using System.IO;
//using System.Runtime.InteropServices;


//namespace Example
//{

//    [Serializable]
//    struct DataPacket
//    {
//        public int type;
//        public string ID;
//        public string PW;
//        public float flo;
//    }

//    [StructLayout(LayoutKind.Sequential)]
//    struct ResultPacket
//    {
//        public int type;
//        public int result;
//        public float flo;
//        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)] public char[] ID_s;
//    }

//    class Program
//    {
//        // 실행 함수
//        static void Main(string[] args)
//        {
//            DataPacket packet = new DataPacket();
//            packet.type = 101;
//            packet.ID = "dnk97";
//            packet.PW = "9738";
//            packet.flo = 5.32f;

//            // 개행 코드
//            //char CR = (char)0x0D;
//            //char LF = (char)0x0A;
//            // 수신 버퍼
//            //StringBuilder sb = new StringBuilder();
//            // 소켓을 연다.
//            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP))
//            {
//                // 로컬의 9090포트로 접속한다.
//                socket.Connect(IPAddress.Parse("127.0.0.1"), 9090);

//                // 수신을 위한 쓰레드
//                //ThreadPool.QueueUserWorkItem((_) =>
//                //{
//                //    while (true)
//                //    {
//                //        // 서버로 오는 메시지를 받는다.
//                //        byte[] ret = new byte[2];
//                //        socket.Receive(ret, 2, SocketFlags.None);
//                //        // 메시지를 unicode로 변환해서 버퍼에 넣는다.
//                //        sb.Append(Encoding.Unicode.GetString(ret, 0, 2));
//                //        // 개행 + \n이면 콘솔 출력한다.
//                //        if (sb.Length >= 4 && sb[sb.Length - 4] == CR && sb[sb.Length - 3] == LF && sb[sb.Length - 2] == '>' && sb[sb.Length - 1] == '\0')
//                //        {
//                //            // 버퍼의 메시지를 콘솔에 출력
//                //            string msg = sb.ToString();
//                //            Console.Write(msg);
//                //            // 버퍼를 비운다.
//                //            sb.Clear();
//                //        }
//                //    }
//                //});
//                // 송신을 위한 입력대기

//                while (true)
//                {
//                    // 콘솔 입력 대기
//                    byte[] buffer = GetBytes_Bind(packet);

//                    // 송신
//                    int sign = socket.Send(buffer, buffer.Length, SocketFlags.None);

//                    Console.Write("데이터 전송 완료");
//                    Console.Write(sign);
//                    Console.Write("\n");


//                    byte[] ret = new byte[1024];
//                    sign = socket.Receive(ret, 1024, SocketFlags.None);

//                    Console.Write("데이터 수신 완료");
//                    Console.Write(sign);
//                    Console.Write("\n");

//                    ResultPacket Rp = new ResultPacket();

//                    Rp = ByteToStruct<ResultPacket>(ret);


//                    Console.Write(Rp.type);
//                    Console.Write("\n");
//                    Console.Write(Rp.ID_s);
//                    Console.Write("\n");
//                    Console.Write(Rp.result);
//                    Console.Write("\n");
//                    Console.Write(Rp.flo);
//                    Console.Write("\n");

//                    //// 메시지를 unicode로 변환해서 버퍼에 넣는다.
//                    break;
//                    // exit 명령어가 오면 종료
//                    //if ("exit".Equals(k, StringComparison.OrdinalIgnoreCase))
//                    //{
//                    //    break;
//                    //}
//                }
//            }
//        }

//        public static T ByteToStruct<T>(byte[] buffer) where T : struct
//        {
//            int size = Marshal.SizeOf(typeof(T));
//            if (size > buffer.Length)
//            {
//                throw new Exception();
//            }

//            IntPtr ptr = Marshal.AllocHGlobal(size);
//            Marshal.Copy(buffer, 0, ptr, size);
//            T obj = (T)Marshal.PtrToStructure(ptr, typeof(T));
//            Marshal.FreeHGlobal(ptr);
//            return obj;
//        }

//        public static byte[] HostToNetworkOrderfloat(float host)
//        {
//            byte[] bytes = BitConverter.GetBytes(host);
//            return bytes;
//        }


//        public const int BODY_BIND_SIZE = 4 + 20 + 20 + 4;

//        // 인증 패킷 구조체를 바이트 배열로 변환하는 함수
//        public static byte[] GetBytes_Bind(DataPacket packet)
//        {
//            byte[] btBuffer = new byte[BODY_BIND_SIZE];

//            MemoryStream ms = new MemoryStream(btBuffer, true);
//            BinaryWriter bw = new BinaryWriter(ms);


//            // Grade - long
//            bw.Write(IPAddress.HostToNetworkOrder(packet.type));

//            // Name - string
//            try
//            {
//                byte[] btName = new byte[20];
//                Encoding.UTF8.GetBytes(packet.ID, 0, packet.ID.Length, btName, 0);
//                bw.Write(btName);
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine("Error : {0}", ex.Message.ToString());
//            }

//            // Subject - string
//            try
//            {
//                byte[] btName = new byte[20];
//                Encoding.UTF8.GetBytes(packet.PW, 0, packet.PW.Length, btName, 0);
//                bw.Write(btName);
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine("Error : {0}", ex.Message.ToString());
//            }

//            bw.Write(HostToNetworkOrderfloat(packet.flo));


//            bw.Close();
//            ms.Close();

//            return btBuffer;
//        }
//    }
//}




//UDP
using System;
using System.Threading;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.InteropServices;


namespace Example
{

    [Serializable]
    struct DataPacket
    {
        public int type;
        public string ID;
        public string PW;
        public float flo;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct ResultPacket
    {
        public int type;
        public int result;
        public float flo;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)] public char[] ID_s;
    }

    class Program
    {
        // 실행 함수
        static void Main(string[] args)
        {
            DataPacket packet = new DataPacket();
            packet.type = 101;
            packet.ID = "dnk97";
            packet.PW = "9738";
            packet.flo = 5.32f;

            // 개행 코드
            //char CR = (char)0x0D;
            //char LF = (char)0x0A;
            // 수신 버퍼
            //StringBuilder sb = new StringBuilder();
            // 소켓을 연다.
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                // 로컬의 9090포트로 접속한다.
                socket.Connect(IPAddress.Parse("127.0.0.1"), 9090);

                // 수신을 위한 쓰레드
                //ThreadPool.QueueUserWorkItem((_) =>
                //{
                //    while (true)
                //    {
                //        // 서버로 오는 메시지를 받는다.
                //        byte[] ret = new byte[2];
                //        socket.Receive(ret, 2, SocketFlags.None);
                //        // 메시지를 unicode로 변환해서 버퍼에 넣는다.
                //        sb.Append(Encoding.Unicode.GetString(ret, 0, 2));
                //        // 개행 + \n이면 콘솔 출력한다.
                //        if (sb.Length >= 4 && sb[sb.Length - 4] == CR && sb[sb.Length - 3] == LF && sb[sb.Length - 2] == '>' && sb[sb.Length - 1] == '\0')
                //        {
                //            // 버퍼의 메시지를 콘솔에 출력
                //            string msg = sb.ToString();
                //            Console.Write(msg);
                //            // 버퍼를 비운다.
                //            sb.Clear();
                //        }
                //    }
                //});
                // 송신을 위한 입력대기

                while (true)
                {
                    // 콘솔 입력 대기
                    byte[] buffer = GetBytes_Bind(packet);

                    // 송신
                    int sign = socket.Send(buffer, buffer.Length, SocketFlags.None);

                    Console.Write("데이터 전송 완료");
                    Console.Write(sign);
                    Console.Write("\n");


                    byte[] ret = new byte[1024];
                    sign = socket.Receive(ret, 1024, SocketFlags.None);

                    Console.Write("데이터 수신 완료");
                    Console.Write(sign);
                    Console.Write("\n");

                    ResultPacket Rp = new ResultPacket();

                    Rp = ByteToStruct<ResultPacket>(ret);


                    Console.Write(Rp.type);
                    Console.Write("\n");
                    Console.Write(Rp.ID_s);
                    Console.Write("\n");
                    Console.Write(Rp.ID_s);
                    Console.Write("\n");
                    Console.Write(Rp.result);
                    Console.Write("\n");
                    Console.Write(Rp.flo);
                    Console.Write("\n");

                    //// 메시지를 unicode로 변환해서 버퍼에 넣는다.
                    break;
                    // exit 명령어가 오면 종료
                    //if ("exit".Equals(k, StringComparison.OrdinalIgnoreCase))
                    //{
                    //    break;
                    //}
                }
            }
        }

        public static T ByteToStruct<T>(byte[] buffer) where T : struct
        {
            int size = Marshal.SizeOf(typeof(T));
            if (size > buffer.Length)
            {
                throw new Exception();
            }

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(buffer, 0, ptr, size);
            T obj = (T)Marshal.PtrToStructure(ptr, typeof(T));
            Marshal.FreeHGlobal(ptr);
            return obj;
        }

        public static byte[] HostToNetworkOrderfloat(float host)
        {
            byte[] bytes = BitConverter.GetBytes(host);
            return bytes;
        }


        public const int BODY_BIND_SIZE = 4 + 20 + 20 + 4;

        // 인증 패킷 구조체를 바이트 배열로 변환하는 함수
        public static byte[] GetBytes_Bind(DataPacket packet)
        {
            byte[] btBuffer = new byte[BODY_BIND_SIZE];

            MemoryStream ms = new MemoryStream(btBuffer, true);
            BinaryWriter bw = new BinaryWriter(ms);


            // Grade - long
            bw.Write(IPAddress.HostToNetworkOrder(packet.type));

            // Name - string
            try
            {
                byte[] btName = new byte[20];
                Encoding.UTF8.GetBytes(packet.ID, 0, packet.ID.Length, btName, 0);
                bw.Write(btName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message.ToString());
            }

            // Subject - string
            try
            {
                byte[] btName = new byte[20];
                Encoding.UTF8.GetBytes(packet.PW, 0, packet.PW.Length, btName, 0);
                bw.Write(btName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message.ToString());
            }

            bw.Write(HostToNetworkOrderfloat(packet.flo));


            bw.Close();
            ms.Close();

            return btBuffer;
        }
    }
}