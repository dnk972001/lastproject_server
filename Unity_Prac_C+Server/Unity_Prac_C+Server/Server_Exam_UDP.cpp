#pragma comment(lib, "ws2_32")
// inet_ntoa가 deprecated가 되었는데.. 사용하려면 아래 설정을 해야 한다.
#pragma warning(disable:4996)
#include <stdio.h>
#include <iostream>
// 소켓을 사용하기 위한 라이브러리
#include <WinSock2.h>

#define PORT 9999   /* 포트 번호 */
#define BUFSIZE 1024
using namespace std;

struct DataPacket {
    int type;
    char ID[20];
    char PW[20];
    float flo;
};

struct ResultPacket {
    int type;
    int result;
    float flo;
    char ID[20];
};

int main()
{
    WSADATA wsdata;
    WSAStartup(MAKEWORD(2, 2), &wsdata);

    SOCKET sock = socket(AF_INET, SOCK_DGRAM, 0);
    SOCKADDR_IN addr;
    ZeroMemory(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(9090);

    bind(sock, (SOCKADDR*)&addr, sizeof(addr));
    cout << "Server Open!" << endl;
    char msg[1024];
    SOCKADDR_IN clntAddr;
    ZeroMemory(&clntAddr, sizeof(clntAddr));
    int clntAddrSz = sizeof(clntAddr);
    recvfrom(sock, msg, sizeof(msg), 0, (SOCKADDR*)&clntAddr, &clntAddrSz);
    cout << "수신한 데이터 크기: " << endl;
    DataPacket* packet = reinterpret_cast<DataPacket*>(msg);

    int type = htonl(packet->type);
    cout << type << endl;
    cout << packet->ID << endl;
    cout << packet->PW << endl;
    cout << packet->flo << endl;
    /* 받은 데이터를 출력 */

    ResultPacket Rp;
    Rp.type = 102;
    char name[20] = "dnk9728";
    memcpy(Rp.ID, name, 20);
    Rp.flo = 2.123f;
    Rp.result = 3;

    const wchar_t* message;
    message = (const wchar_t*)&Rp;


    sendto(sock, (char *)message, sizeof(Rp), 0, (SOCKADDR*)&clntAddr, sizeof(clntAddr));

    cout << "데이터 전송 완료!" << endl;
    /* 받은 데이터를 클라이언트에게 보냄 */
    //if (recvLen = sendto(seversocket, (char *)message,  sizeof(Rp), 0, (struct sockaddr*)&clntAddr, sizeof(clntAddr)) != sizeof(Rp)) {
    //    perror("sendto failed");
    //    exit(1);
    //}
    //recvLen = sendto(seversocket, (char*)message, sizeof(Rp), 0, (struct sockaddr*)&clntAddr, sizeof(clntAddr)) != sizeof(Rp);
    //cout << "데이터 송신:" << recvLen << endl;
   // }
}
